import p1 from './assets/images/towerofhanoi.png'
import p2 from './assets/images/todolist.png'
import p3 from './assets/images/awesomebooks.png'

const  projects = [
    {   
        id:0,
        img:p1,
        name:'Tower of hanoi',
        discription: 'The Tower of Hanoi is a classic game of logical thinking and sequential reasoning. Move rings from one tower to another but make sure you follow the rules! ,built with vanilla JavaScript',
        liveDomo: 'https://yasinabdmahmood.github.io/Tower-of-Hanoi/src/',
        gitHubLink: 'https://github.com/yasinabdmahmood/Tower-of-Hanoi',
        technologies:['HTML5','CSS3','JavaScript']
    },
    {   
        id:1,
        img:p2,
        name:'To do List',
        discription: 'The Tower of Hanoi is a classic game of logical thinking and sequential reasoning. Move rings from one tower to another but make sure you follow the rules! ,built with vanilla JavaScript',
        liveDomo: 'https://yasinabdmahmood.github.io/To-do-list/dist/',
        gitHubLink: 'https://github.com/yasinabdmahmood/To-do-list',
        technologies:['HTML5','CSS3','JavaScript']
    },
    {   
        id:2,
        img:p3,
        name:'Awesome books',
        discription: 'The Tower of Hanoi is a classic game of logical thinking and sequential reasoning. Move rings from one tower to another but make sure you follow the rules! ,built with vanilla JavaScript',
        liveDomo: 'https://yasinabdmahmood.github.io/Awesome-books/',
        gitHubLink: 'https://github.com/yasinabdmahmood/Awesome-books',
        technologies:['HTML5','CSS3','JavaScript']
    },
    
 ]

 export default projects